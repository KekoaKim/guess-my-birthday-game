from random import randint

user_name = input("Hi! What is your name? ")

range_one = range(5)
for num in range_one:
    month = str(randint(1,12))
    year = str(randint(1960, 2004))
    guess = month + " / " + year
    num = str(num + 1)
    print("Guess ", num,  ": ", user_name, " were you born in", guess )
    response = input("Yes or No? ")
    if response == "Yes":
        print("I knew it!")
        exit()
    else:
        if num == "5":
            print("I have other things to do. Goode bye.")
        else:
            print("Drat! Let me try again!")
